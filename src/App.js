import React, { Component } from "react";
import Landing from './components/Landing';
import LoginPage from './components/Login';
import SignupPage from './components/Signup';
import HomePage from './components/Home';
import PageNotFound from './components/PageNotFound';
import { browserHistory } from 'react-router';
import LocalSetting from './AppConfig.json';

window.ip = LocalSetting.IP;


//redirect page as per the URL in Browser
function renderLogin(isLogin){
    let redirection = "";
    if(window.location.pathname.includes("/login")){
        if (isLogin){
            redirection = <Landing/>
            browserHistory.push('/');
        }
        else{
            redirection = <LoginPage/>
        }
    }
    else if(window.location.pathname.includes("/signup")){
        if (isLogin){
            redirection = <Landing/>
            browserHistory.push('/');
        }
        else{
            redirection = <SignupPage/>;
        }
    }
    else if(window.location.pathname.includes("/dashboard")){
        if (isLogin){
            redirection = <HomePage/>
        }
        else{
            redirection = <Landing/>;
        }
    }
    else if(window.location.pathname === "/"){
        redirection = <Landing/>;
    }
    else{
        // if any other URL hit
        redirection = <PageNotFound/>;
    }
    return redirection;
}


class Login extends Component {
  render(){
    return (
      // if user is logged in then redirect to Homepage or to login page
      localStorage.getItem("Token") && localStorage.getItem("uid") ? (
          renderLogin(true)
      ) : (
          renderLogin(false)
      )
    );
  }
}

export default Login;