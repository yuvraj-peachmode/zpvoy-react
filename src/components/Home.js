import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
import Landing from './Landing';
import axios from 'axios';
import '../index.css';

class home extends Component {
  constructor(props){
    super(props);
    this.state = {
        apiKey : "",
        sessionKey : "",
        host : ""
    };
    this.logOut = this.logOut.bind(this);
  }
  // get API key for display purpose
  getAPIKey(){

    var headers = {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem("Token")
    }
    var udata = {'uid' :localStorage.getItem("uid")};
    axios.post(window.ip+"/api/dashboard/", udata, {headers: headers})
    .then(res => {
        this.setState({ apiKey: res.data["api_key"] });
    })
    .catch(error =>{
        if(error.response.status === 440 || error.response.data.error === 'User not exists.'){
            localStorage.removeItem("uid");
            localStorage.removeItem("Token");
            // redirect to Before login
            ReactDOM.render( <Landing />, document.getElementById('root'));
            browserHistory.push('/');
        }

    });
  }
  // on logout
  logOut = event => {
    event.preventDefault();
    // params to send in API call
    let headers = {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem("Token"),
    }
    // API call for logOut functionality
    axios.post(window.ip+"/api/logout/", {} , {headers: headers})
      .then(res => {
        // delete localStorage on logout
        localStorage.removeItem("uid");
        localStorage.removeItem("Token");
        // redirect to Before login
        ReactDOM.render( <Landing />, document.getElementById('root'));
        browserHistory.push('/');
    })
    .catch(error => {
        //console.log(error);
    });

  }
  // function will be called only if page refreshes
  handleLoad(){
    var headers = {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem("Token")
    }
    // API call to check session status
    var udata = {'uid' :localStorage.getItem("uid")};
    axios.post(window.ip+"/api/dashboard/", udata, {headers: headers})
      .then(res => {
        //console.log(res.data);
    })
    .catch(error => {
        document.getElementById("logout").click();
        //this.logOut();
    });
  }
  // once component loaded this methods will be called
  componentDidMount(){
    this.getAPIKey();
    //window.addEventListener('load', this.handleLoad);
  }
  render() {
    // if user login
    if(localStorage.getItem("Token") && localStorage.getItem("uid")){
        return (
          <div className="container">
            <form className="mt-0">
              <div className="row justify-content-center">
                <div className="col-md-7 col-sm-12">
                    <div className="card">
                      <div className="card-body">
                        <p><b>The gender api :</b> {window.ip}/v1/?name=&lt;input_name&gt;&key=&lt;api_key&gt; </p>
                        <p><b>Your api key :</b> {this.state.apiKey} </p>
                        <button onClick={this.logOut} id="logout" className="btn btn-primary">Logout</button>
                      </div>
                    </div>
                </div>
              </div>
            </form>
          </div>
        );
    }else{
    // if user is not login
        return (
          <Landing/>
        );
    }
  }
}

export default home;
