import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
import Home from './Home';
import Login from './Login';
import axios from 'axios';


class signup extends Component {
  constructor(){
    super();
    this.state = {
        email : '',
        pwd   : ''
    };
    this.gotoLogin = this.gotoLogin.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.emailHandleChange = this.emailHandleChange.bind(this);
    this.pwdHandleChange = this.pwdHandleChange.bind(this);
  }
  // redirect to login page
  gotoLogin(){
    ReactDOM.render( <Login />, document.getElementById('root'));
    browserHistory.push('/login');
  }

    // email onchange function
    emailHandleChange(event) {
        this.setState({ email: event.target.value});
    }
    // password onchange function
    pwdHandleChange(event) {
        this.setState({ pwd: event.target.value});
    }

  // on Sign up form submit
  submitForm = event => {
    event.preventDefault();
    // if all fields are proper then
    const params = JSON.stringify({
        email : this.state.email,
        password : this.state.pwd
    });
    // API call to signup
    //axios.post("http://localhost:8000/api/users/", JSON.parse(params))
    axios.post(window.ip+"/api/users/", JSON.parse(params))
      .then(res => {
        // redirect to login on success
        var resData = JSON.stringify(res.data);
        localStorage.setItem('uid', JSON.parse(resData)['id']);
        localStorage.setItem('Token', JSON.parse(resData)['session_key']);
        ReactDOM.render( <Home />, document.getElementById('root'));
        browserHistory.push('/dashboard');
    })
    .catch(error => {
        // if error occurs
        let element = document.getElementById("error");
        element.classList.remove("blank");
        element.classList.add("alert-danger");
        if(error.response.status === 409){
            element.innerHTML = "Email already exist";
        }else{
            element.innerHTML = "Something went wrong.";
        }
    });
  }
  render() {
    return (
      <div className="container">
        <form className="mt-0" onSubmit={this.submitForm}>
          <div className="row justify-content-center">
            <div className="col-md-6 col-sm-12">
                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title mb-4 text-center">Welcome to the Gender API</h4>
                    <h6 className="card-subtitle mb-2 text-center text-muted">Please sign up below to get started.</h6>
                    <div className="alert alert-error blank" id="error" role="alert"></div>
                      <div className="form-group">
                        <label>Email address</label>
                        <input type="email" className="form-control" id="email" placeholder="Enter email" required="required" value={this.state.email} onChange={this.emailHandleChange} />
                      </div>
                      <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control" id="password" placeholder="Password" required="required" value={this.state.pwd} onChange={this.pwdHandleChange} pattern=".{6,}" title="6 characters minimum" />
                      </div>
                      <button type="submit" className="btn btn-primary btn-block mb-4">Sign Up</button>
                      <p className="text-center mb-0">Already have an account? <button type="button" onClick={this.gotoLogin} className="btn-link pointer">Log in</button>.</p>
                    </div>
                  </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default signup;