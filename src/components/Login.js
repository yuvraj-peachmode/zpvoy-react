import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
import Home from './Home';
import Signup from './Signup';
import axios from 'axios';
import '../index.css';

class Login extends Component {
    constructor(){
        super();
        this.state = {
            email : "",
            pwd : "",
            error: "",
            redirectToReferrer: false
        };
        this.gotoSignUp = this.gotoSignUp.bind(this);
        this.loginUser = this.loginUser.bind(this);
        this.emailHandleChange = this.emailHandleChange.bind(this);
        this.pwdHandleChange = this.pwdHandleChange.bind(this);
    }
    // redirect to signup page
    gotoSignUp(){
      ReactDOM.render( <Signup />, document.getElementById('root'));
      browserHistory.push('/signup');
    }
    // email field on change function
    emailHandleChange(event) {
        this.setState({ email: event.target.value});
    }
    // password field on change function
    pwdHandleChange(event) {
        this.setState({ pwd: event.target.value});
    }
  // Login function
  loginUser = event => {
    event.preventDefault();
    // params to send in API call
    const params = JSON.stringify({
        email : this.state.email,
        password : this.state.pwd
    });

    // API call for login functionality
    axios.post(window.ip+"/api/login/", JSON.parse(params))
      .then(res => {
        // Storing session key api key and user id in local storage
        var resData = JSON.stringify(res.data);
        var uid = JSON.parse(resData)['id'];
        var token = JSON.parse(resData)['session_key'];
        localStorage.setItem('uid', uid);
        localStorage.setItem('Token', token);
        // redirect to Dashboard
        ReactDOM.render( <Home />, document.getElementById('root'));
        browserHistory.push('/dashboard');
    })
      .catch(error => {
        // if error occurs
        let element = document.getElementById("error");
        element.classList.remove("blank");
        element.classList.add("alert-danger");
        element.innerHTML = error.response.data.message;
//        element.innerHTML = "Something went wrong.";
        //console.log(error);
      });

    }

  render() {

    return (
      <div className="container">
        <form className="mt-0" onSubmit={this.loginUser}>
          <div className="row justify-content-center">
            <div className="col-md-6 col-sm-12">
                <div className="card">
                  <div className="card-body">
                    <h4 className="card-title mb-4 text-center">Welcome back to the Gender API</h4>
                    <h6 className="card-subtitle mb-2 text-center text-muted">Please Login below.</h6>
                    <div className="alert alert-error blank" id="error" role="alert">{this.state.error}</div>
                    <div className="form-group">
                        <label>Email address</label>
                        <input type="email" className="form-control" id="email" placeholder="Enter Email" required="required" value={this.state.email} onChange={this.emailHandleChange} />
                      </div>
                      <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control" id="password" placeholder="Password" required="required" value={this.state.pwd} onChange={this.pwdHandleChange} />
                      </div>
                      <button type="submit" className="btn btn-primary btn-block mb-4">Sign In</button>
                      <p className="text-center mb-0">Dont have an account? <button type="button" onClick={this.gotoSignUp} className="btn-link pointer">Sign Up</button>.</p>
                    </div>
                  </div>
            </div>
          </div>
        </form>
      </div>
    );

  }
}

export default Login;
