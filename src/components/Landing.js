import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
import Login from './Login';
import Home from './Home';
import axios from 'axios';
import '../index.css';


class before_login extends Component {
  // redirect to login
  login(){
    ReactDOM.render( <Login />, document.getElementById('root'));
    browserHistory.push('/login');
  }
  // redirect to dashboard
  dashboard(){
    ReactDOM.render( <Home />, document.getElementById('root'));
    browserHistory.push('/dashboard');
  }

  // function will be called only if page refreshes
  handleLoad(){
    var headers = {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem("Token")
    }
    // API call to check session status
    var udata = {'uid' :localStorage.getItem("uid")};
    axios.post(window.ip+"/api/dashboard/", udata, {headers: headers})
      .then(res => {

    })
    .catch(error => {
        if(error.response.status === 440){
            localStorage.removeItem("uid");
            localStorage.removeItem("Token");
            ReactDOM.render( <Login />, document.getElementById('root'));
            browserHistory.push('/login');
        }
    });
  }
  // once component loaded this methods will be called
  componentDidMount(){
    if(localStorage.getItem("Token") && localStorage.getItem("uid")){
        window.addEventListener('load', this.handleLoad);
    }
  }
  render() {
    // On user login
    if(localStorage.getItem("Token") && localStorage.getItem("uid")){
      return(
        <div className="card border-0">
            <div className="card-body">
                <h4 className="card-title mb-4 text-center">Welcome to the Gender API</h4>
                <h6 className="card-subtitle mb-2 text-center text-muted">Please <button onClick={this.dashboard.bind(this)} className="btn-link pointer">go to Dashboard</button> to get started.</h6>
            </div>
        </div>
      );
    }else{
    // on user Signup
      return(
        <div className="card border-0">
            <div className="card-body">
                <h4 className="card-title mb-4 text-center">Welcome to the Gender API</h4>
                <h6 className="card-subtitle mb-2 text-center text-muted">Please <button onClick={this.login.bind(this)} className="btn-link pointer">Log in</button> to get started.</h6>
            </div>
        </div>
      );
    }
  }
}

export default before_login;