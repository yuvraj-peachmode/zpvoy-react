import React, { Component } from 'react';

class pageNotFound extends Component {
  render() {
    return (
      <div className="container">
        <h1 className="text-center"><b>404</b><br/>Page Not Found</h1>
      </div>
    );

  }
}

export default pageNotFound;
